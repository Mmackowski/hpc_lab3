#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

int main(int argc,char * argv[])
{
	long unsigned int Counter=10000,i,number=0;
	double pi;
	int chunk=0;
	srand(time(NULL));
	int k = 0;
	struct timespec tstart={0,0}, tend={0,0};
	printf(" M = [");
	for(k = 0; k<10;++k)
	{
	number = 0;
	chunk += Counter/10;
	clock_gettime(CLOCK_MONOTONIC, &tstart);
	#pragma omp parallel for shared(Counter) private(i) schedule(static, chunk) reduction(+:number)
		for(i=0;i<Counter;i++)
		{	
			float x,y;
			x =(float) rand()/RAND_MAX;
			y = (float) rand()/RAND_MAX;	
			
			if((pow(x,2)+pow(y,2))<=1)
			{
			 	number = number+1;
			}
		}
	clock_gettime(CLOCK_MONOTONIC, &tend);
	printf(" %.5f",
           ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - 
           ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec));
	}
	printf("]\n");
	chunk = 0;
	printf("X = [");
	for(k=0;k<10;++k)
	{
		chunk += Counter/10;
		printf(" %d",chunk);
	}
	printf("]\n");
	//pi = 4 * (double)  number/Counter;
	//printf("liczba pi wynosi %f \n",pi);

	return 0;
}
